/**
 * Returns document dimensions based on input. Allows for custom dimensions if none of the
 * hard-coded formats are what is desired.
 *
 * @param format: String which determines PDF page dimensions. Otherwise a tuple containing
 * custom dimensions.
 * @param element: HTML element subject to formatting.
 * @param dpi: Number representing the dots-per-inch of the generated file.
 */
export default function resolveFormat(format, element = undefined, dpi = undefined) {
  if (format === 'automatic') {
    if (element == null) {
      throw new Error('Invalid Element: the target element must be defined when format is automatic');
    }
    if (dpi == null) {
      throw new Error('Invalid DPI: dpi must be defined when format is automatic');
    }
    const targetArea = (dpi * 8.5) * (dpi * 11); // target area is the pixel density of an 8.5 by 11 page at the given dpi
    const width = element.offsetWidth;
    const height = targetArea / width;
    return [width / 100, height / 100];
  }
  switch ((typeof format === 'string') ? format.toLowerCase() : format) {
    case 'letter':
    case 'a4':
      return [8.5, 11.0];
    case 'ledger':
    case 'tabloid':
    case 'a3':
      return [11.0, 17.0];
    case 'a2':
      return [17.0, 22.0];
    case 'a1':
      return [22.0, 34.0];
    case 'a0':
      return [34.0, 44.0];
    case 'half-letter':
      return [5.5, 8.5];
    case 'legal':
      return [8.5, 14.0];
    case 'junior-legal':
      return [5.0, 8.0];
    default:
      if (Array.isArray(format) && format.length === 2) {
        return [format[0], format[1]];
      }
      throw new Error(`Invalid Type: expected 'format' to be of type ['number', 'number'] but instead found '${typeof format}.'`);
  }
}
