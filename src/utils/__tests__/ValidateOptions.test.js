import validateOptions from '../ValidateOptions';
import { DefaultOptions } from '../../constants/DefaultOptions';
import { OptionErrors } from '../../constants/DocumentGeneratorErrors';

describe('validateOptions function from ValidateOptions.js', () => {
  let options = {};

  beforeEach(() => {
    options = {};
    const keys = Object.keys(DefaultOptions);
    keys.forEach((key) => {
      options[key] = DefaultOptions[key];
    });
  });

  it('Should throw an error when filename is not a string', () => {
    options.filename = 12345;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.filename.invalidType(typeof options.filename));
  });

  it('Should throw an error when outputType is not a string', () => {
    options.outputType = 12345;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.outputType.invalidType(typeof options.outputType));
  });

  it('Should throw an error when format is neither a string nor number array', () => {
    options.format = 12345;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.format.invalidType(typeof options.format));
  });

  it('Should throw an error when dpi is not a number', () => {
    options.dpi = 'word';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.dpi.invalidType(typeof options.dpi));
  });

  it('Should throw an error when compression is not a string', () => {
    options.compression = 12345;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.compression.invalidType(typeof options.compression));
  });

  it('Should throw an error when continuous is not a boolean', () => {
    options.continuous = 12345;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.continuous(typeof options.continuous));
  });

  it('Should throw an error when bottomMargin is not a number', () => {
    options.bottomMargin = 'word';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.bottomMargin(typeof options.bottomMargin));
  });

  it('Should throw an error when scrollOffset is not a number', () => {
    options.scrollOffset = 'word';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.scrollOffset(typeof options.scrollOffset));
  });

  it('Should throw an error when imageScale is not a number', () => {
    options.imageScale = 'word';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.imageScale.invalidType(typeof options.imageScale));
  });

  it('Should throw an error when imageQuality is not a number', () => {
    options.imageQuality = 'word';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.imageQuality.invalidType(typeof options.imageQuality));
  });

  it('Should throw an error when filename has a bad file extension', () => {
    options.filename = 'output.png';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.filename.badExtension(options.filename));
  });

  it('Should throw an error when outputType is not a real output type', () => {
    options.outputType = 'sandwich';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.outputType.invalidOutput(options.outputType));
  });

  it('Should throw an error when format is a string but an invalid option', () => {
    options.format = 'notepad';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.format.invalidFormatString(options.format));
  });

  it('Should throw an error when format is an array but too short', () => {
    options.format = [50];
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.format.invalidFormatArray(options.format));
  });

  it('Should throw an error when format is an array but too long', () => {
    options.format = [5, 3, 8];
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.format.invalidFormatArray(options.format));
  });

  it('Should throw an error when dpi is not an integer', () => {
    options.dpi = 3.14759;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.dpi.invalidNumber(options.dpi));
  });

  it('Should throw an error when dpi is less than 1', () => {
    options.dpi = 0;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.dpi.invalidRange(options.dpi));
  });

  it('Should throw an error when compression is not a valid compression type', () => {
    options.compression = 'REALLY SLOW';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.compression.invalidCompression(options.compression));
  });

  it('Should throw an error when imageScale is less than 0', () => {
    options.imageScale = -0.95;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.imageScale.invalidRange(options.imageScale));
  });

  it('Should throw an error when imageScale is 0', () => {
    options.imageScale = 0;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.imageScale.invalidRange(options.imageScale));
  });

  it('Should throw an error when imageScale is greater than 1', () => {
    options.imageScale = 1.5;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.imageScale.invalidRange(options.imageScale));
  });

  it('Should throw an error when imageQuality is 0', () => {
    options.imageQuality = 0;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.imageQuality.invalidRange(options.imageQuality));
  });

  it('Should throw an error when imageQuality is greater than 1', () => {
    options.imageQuality = 1.5;
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.imageQuality.invalidRange(options.imageQuality));
  });

  it('Should throw an error when given an invalid options object', () => {
    options = 'These are my options!';
    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.invalidType(typeof options));
  });

  it('Should throw an error when passed an options object with an invalid option', () => {
    options = {
      filename: 'output.pdf',
      outputType: 'save',
      format: 'automatic',
      dpi: 100,
      compression: 'FAST',
      continuous: true,
      bottomMargin: 0,
      scrollOffset: 0,
      imageScale: 0.95,
      fakeOption: 'This Option Does Not Exist!',
    };

    expect(() => {
      validateOptions(options);
    }).toThrow(OptionErrors.badOption('fakeOption'));
  });

  it('Should return true when given valid options (and format is a string)', () => {
    const result = validateOptions(options);
    expect(result).toBe(true);
  });

  it('Should return true when given valid options (and format is an array)', () => {
    options.format = [18, 32];
    const result = validateOptions(options);
    expect(result).toBe(true);
  });

  it('Should return true when given incomplete, but valid options', () => {
    options = {
      filename: 'example.pdf',
      dpi: 200,
      imageScale: 0.9,
    };

    const keys = Object.keys(options);
    expect(keys.length).toBe(3);

    const result = validateOptions(options);
    expect(result).toBe(true);
  });

  it('Should return false when not given an options are not truthy', () => {
    const noOptions = validateOptions();
    const nullOptions = validateOptions(null);
    const undefinedOptions = validateOptions(undefined);

    expect(noOptions).toBe(false);
    expect(nullOptions).toBe(false);
    expect(undefinedOptions).toBe(false);
  });
});
