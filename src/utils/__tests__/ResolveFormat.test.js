import resolveFormat from '../ResolveFormat';

describe('resolveFormat function from ResolveFormat.js', () => {
  it('Should resolve standard page dimensions', () => {
    let result = resolveFormat('letter');
    expect(result).toEqual([8.5, 11.0]);
    expect(result).toEqual(resolveFormat('a4'));

    result = resolveFormat('ledger');
    expect(result).toEqual([11.0, 17.0]);
    expect(result).toEqual(resolveFormat('tabloid'));
    expect(result).toEqual(resolveFormat('a3'));

    result = resolveFormat('a2');
    expect(result).toEqual([17.0, 22.0]);

    result = resolveFormat('a1');
    expect(result).toEqual([22.0, 34.0]);

    result = resolveFormat('a0');
    expect(result).toEqual([34.0, 44.0]);

    result = resolveFormat('half-letter');
    expect(result).toEqual([5.5, 8.5]);

    result = resolveFormat('legal');
    expect(result).toEqual([8.5, 14.0]);

    result = resolveFormat('junior-legal');
    expect(result).toEqual([5.0, 8.0]);
  });

  it('Should resolve custom page dimensions', () => {
    const result = resolveFormat([10.0, 10.0]);
    expect(result).toEqual([10.0, 10.0]);
  });

  it('Should reject invalid page dimensions', () => {
    const invalidInput = 400;
    const err = `Invalid Type: expected 'format' to be of type ['number', 'number'] but instead found '${typeof invalidInput}.'`;

    expect(() => {
      resolveFormat(invalidInput);
    }).toThrow(new Error(err));
  });

  it('Should reject automatic scaling without a target element', () => {
    const err = 'Invalid Element: the target element must be defined when format is automatic';

    expect(() => {
      resolveFormat('automatic');
    }).toThrow(new Error(err));
  });

  it('Should reject automatic scaling without a provided dpi', () => {
    document.body.innerHTML = '<div id="toPDF">This Is Example Text</div>';
    const element = document.getElementById('toPDF');
    const err = 'Invalid DPI: dpi must be defined when format is automatic';

    expect(() => {
      resolveFormat('automatic', element);
    }).toThrow(new Error(err));
  });
});