import { OptionErrors } from '../constants/DocumentGeneratorErrors';

export default function validateOptions(options): boolean {
  if (typeof options !== 'object' && options) {
    throw OptionErrors.invalidType(typeof options);
  } else if (!options) {
    return false;
  }

  const keys = Object.keys(options);

  keys.forEach((key) => {
    switch (key) {
      case 'filename':
        const filename = options.filename;

        if(typeof filename !== 'string') {
          throw OptionErrors.filename.invalidType(typeof filename);
        }

        const extension = filename.split('.')[1];

        if (extension !== 'pdf') {
          throw OptionErrors.filename.badExtension(filename);
        }
        break;

      case 'outputType':
        const outputType = options[key];

        if (typeof outputType !== 'string') {
          throw OptionErrors.outputType.invalidType(typeof outputType);
        }

        const validOutputTypes = [
          'arraybuffer',
          'blob',
          'bloburi',
          'bloburl',
          'datauristring',
          'dataurlstring',
          'datauri',
          'dataurl',
          'dataurlnewwindow',
          'pdfobjectnewwindow',
          'pdfjsnewwindow',
          'save',
        ];

        if (!validOutputTypes.includes(outputType)) {
          throw OptionErrors.outputType.invalidOutput(outputType);
        }
        break;

      case 'format':
        const format = options[key];

        if (typeof format !== 'string' && !Array.isArray(format)) {
          throw OptionErrors.format.invalidType(typeof format);
        }

        const formatStrings = [
          'automatic',
          'letter',
          'a4',
          'ledger',
          'tabloid',
          'a3',
          'a2',
          'a1',
          'a0',
          'half-letter',
          'legal',
          'junior-legal'
        ];

        if (typeof format === 'string' && !formatStrings.includes(format)) {
          throw OptionErrors.format.invalidFormatString(format);
        } else if (Array.isArray(format) && format.length !== 2) {
          throw OptionErrors.format.invalidFormatArray(format);
        }
        break;

      case 'dpi':
        const dpi = options[key];

        if (typeof dpi !== 'number') {
          throw OptionErrors.dpi.invalidType(typeof dpi);
        } else if (!Number.isInteger(dpi)) {
          throw OptionErrors.dpi.invalidNumber(dpi);
        } else if (dpi < 1) {
          throw OptionErrors.dpi.invalidRange(dpi);
        }
        break;

      case 'compression':
        const compression = options[key];
        const compressionStrings = ['NONE', 'FAST', 'MEDIUM', 'SLOW'];

        if (typeof compression !== 'string') {
          throw OptionErrors.compression.invalidType(typeof compression);
        } else if (!compressionStrings.includes(compression)) {
          throw OptionErrors.compression.invalidCompression(compression);
        }
        break;

      case 'continuous':
        const continuous = options[key];
        if (typeof continuous !== 'boolean') {
          throw OptionErrors.continuous(typeof continuous);
        }
        break;

      case 'bottomMargin':
        const bottomMargin = options[key];
        if (typeof bottomMargin !== 'number') {
          throw OptionErrors.bottomMargin(typeof bottomMargin);
        }
        break;

      case 'scrollOffset':
        const scrollOffset = options[key];
        if (typeof scrollOffset !== 'number') {
          throw OptionErrors.scrollOffset(typeof scrollOffset);
        }
        break;

      case 'imageScale':
        const imageScale = options[key];

        if (typeof imageScale !== 'number') {
          throw OptionErrors.imageScale.invalidType(typeof imageScale);
        } else if (imageScale <= 0 || imageScale > 1) {
          throw OptionErrors.imageScale.invalidRange(imageScale);
        }
        break;

      case 'imageQuality':
        const imageQuality = options[key];

        if (typeof imageQuality !== 'number') {
          throw OptionErrors.imageQuality.invalidType(typeof imageQuality);
        } else if (imageQuality <= 0 || imageQuality > 1) {
          throw OptionErrors.imageQuality.invalidRange(imageQuality);
        }
        break;

      default:
        throw OptionErrors.badOption(key);
    }
  });
  return true;
}
