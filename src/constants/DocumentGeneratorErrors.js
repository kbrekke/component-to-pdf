
function invalidOptionTypeException(opt, expected, invalid) {
  return new Error(`Invalid Type: option '${opt}' is expected to be of type '${expected}' but was found to be of type '${invalid}' instead`);
}

export const OptionErrors = {
  filename: {
    badExtension: ((filename) => new Error(`Bad Extension: '${filename}' is not a valid file name, because it must end in '.pdf'`)),
    invalidType: ((invalidType) => invalidOptionTypeException('filename', 'string', invalidType)),
  },
  outputType: {
    invalidOutput: ((outputType) => new Error(`Invalid Output Type: '${outputType}' is not a recognized output type`)),
    invalidType: ((invalidType) => invalidOptionTypeException('outputType', 'string', invalidType)),
  },
  format: {
    invalidFormatArray: ((format) => new Error(`Invalid Format Array: '${format}' is not a valid format, expected to be ['number', 'number']`)),
    invalidFormatString: ((format) => new Error(`Invalid Format String: '${format}' is not a recognized format type`)),
    invalidType: ((invalidType) => new Error(`Invalid Type: option 'format' is expected to be of type 'string' or of type 'number[]', but was found to be of type '${invalidType}' instead`)),
  },
  dpi: {
    invalidRange: ((dpi) => new Error(`Invalid Range: ${dpi} is not valid dpi because it must be greater than or equal to 1`)),
    invalidNumber: ((dpi) => new Error(`Invalid Number: ${dpi} is not a valid dpi because it must be an integer`)),
    invalidType: ((invalidType) => invalidOptionTypeException('dpi', 'number', invalidType)),
  },
  compression: {
    invalidCompression: ((compression) => new Error(`Invalid Compression: '${compression}' is not a recognized compression type`)),
    invalidType: ((invalidType) => invalidOptionTypeException('compression', 'string', invalidType)),
  },
  continuous: ((invalidType) => invalidOptionTypeException('continuous', 'boolean', invalidType)),
  bottomMargin: ((invalidType) => invalidOptionTypeException('bottomMargin', 'number', invalidType)),
  scrollOffset: ((invalidType) => invalidOptionTypeException('scrollOffset', 'number', invalidType)),
  imageScale: {
    invalidRange: ((imageScale) => new Error(`Invalid Range: ${imageScale} is not a valid imageScale because it must be between 0 and 1`)),
    invalidType: ((invalidType) => invalidOptionTypeException('imageScale', 'number', invalidType)),
  },
  imageQuality: {
    invalidRange: ((imageQuality) => new Error(`Invalid Range: ${imageQuality} is not a valid imageQuality because it must be between 0 and 1`)),
    invalidType: ((invalidType) => invalidOptionTypeException('imageQuality', 'number', invalidType)),
  },
  badOption: ((option) => new Error(`Bad Option: '${option}' is not a valid option, but was provided as one`)),
  invalidType: ((invalidType) => new Error(`Invalid Type: generator options must be a set of key value pairs, but were instead found to be of type '${invalidType}'`)),
};