import DocumentGenerator from '../DocumentGenerator';
import { DefaultOptions } from '../../constants/DefaultOptions';
import { OptionErrors } from '../../constants/DocumentGeneratorErrors';

describe('DocumentGenerator class from DocumentGenerator.js', () => {
  let docGen;

  beforeEach(() => {
    docGen = new DocumentGenerator();
  });

  it('Should have default options when instantiated from an empty constructor', () => {
    const keys = Object.keys(DefaultOptions);

    keys.forEach((key) => {
      expect(docGen.options[key]).toBe(DefaultOptions[key]);
    });
  });

  it('Should replace its default options when new values are passed to its constructor', () => {
    const newOptions = {
      filename: 'new_output.pdf',
      outputType: 'dataurl',
      format: 'a4',
      dpi: 72,
      compression: 'NONE',
      continuous: false,
      bottomMargin: 0.1,
      scrollOffset: 50,
      imageScale: 0.5,
    };
    docGen = new DocumentGenerator(newOptions);
    const keys = Object.keys(newOptions);

    keys.forEach((key) => {
      expect(docGen.options[key]).toBe(newOptions[key]);
    });
  });

  it('Should replace specific values when given incomplete options, but keep all other values as their defaults', () => {
    const incompleteOptions = {
      filename: 'new_output.pdf',
      dpi: 200,
      compression: 'SLOW',
      bottomMargin: 0.2,
    };
    docGen = new DocumentGenerator(incompleteOptions);
    const keys = Object.keys(DefaultOptions);

    keys.forEach((key) => {
      if (incompleteOptions.hasOwnProperty(key)) {
        expect(docGen.options[key]).toBe(incompleteOptions[key]);
      } else {
        expect(docGen.options[key]).toBe(DefaultOptions[key]);
      }
    });
  });

  it('Should have default values, which are replaced after using setOptions', () => {
    const newOptions = {
      filename: 'new_output.pdf',
      outputType: 'dataurl',
      format: 'a4',
      dpi: 72,
      compression: 'NONE',
      continuous: false,
      bottomMargin: 0.1,
      scrollOffset: 50,
      imageScale: 0.5,
      imageQuality: 1.0,
    };
    const keys = Object.keys(DefaultOptions);

    keys.forEach((key) => {
      expect(docGen.options[key]).toBe(DefaultOptions[key]);
    });

    docGen.setOptions(newOptions);
    keys.forEach((key) => {
      expect(docGen.options[key]).toBe(newOptions[key]);
    });
  });

  it('Should throw an error when given a non-existent option in constructor', () => {
    expect(() => {
      new DocumentGenerator({ invalidOption: 'SNAFU' });
    }).toThrow(OptionErrors.badOption('invalidOption'));
  });

  it('Should throw an error when given a non-existent option using setOptions', () => {
    expect(() => {
      docGen.setOptions({ invalidOption: 'SNAFU' });
    }).toThrow(OptionErrors.badOption('invalidOption'));
  });

  it('Should reject when given a non-existent option using GetPDFById', async () => {
    try {
      await docGen.getPDFById('notImportant', {invalidOption: 'SNAFU'});
    } catch(e) {
      expect(e).toEqual(OptionErrors.badOption('invalidOption'));
    }
  });

  it('Should create a pdf from html element through getPDFById', async () => {
    window.alert = jest.fn();

    const id = 'toPDF';
    document.body.innerHTML = `<div id=${id}>This Is Example Text</div>`;

    const result = await docGen.getPDFById(id);
    expect(result).toBeTruthy();
  });

  it('Should return a pdf from html element through getPDFByElement', async () => {
    window.alert = jest.fn();

    const id = 'toPDF';
    document.body.innerHTML = `<div id=${id}>This is Example Text</div>`;

    const result = await docGen.getPDFByElement(document.getElementById(id));
    expect(result).toBeTruthy();
  });

  it('Should return null if getPDFById cannot find the target element', async () => {
    window.alert = jest.fn();

    const id = 'toPDF';
    document.body.innerHTML = '<div>This is Example Text</div>';

    const result = await docGen.getPDFById(id);
    expect(result).toBe(null);
  });
});
