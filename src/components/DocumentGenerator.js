import resolveFormat from '../utils/ResolveFormat';
import validateOptions from '../utils/ValidateOptions'
import { DefaultOptions } from '../constants/DefaultOptions';

export default class DocumentGenerator {
  constructor(options = undefined) {
    this._documentSettings = {
      image: {
        source: undefined,
        xStart: 0,
        yStart: 0,
        width: 0,
        height: 0,
        xOrigin: 0,
        yOrigin: 0,
      },
      page: {
        height: 0,
        width: 0,
        ratio: 0,
      }
    };

    this.options = DefaultOptions;
    if (options) {
      this.setOptions(options);
    }
  }

  setOptions(options) {
    if (validateOptions(options)) {
      const keys = Object.keys(options);
      keys.forEach((key) => this.options[key] = options[key]);
    }
  }

  /**
   * getPDFById converts an element and its contents into a pdf by creating a canvas, drawing the
   * content of the element onto the canvas, and then inserting the produced image into a
   * generated pdf file. The function does this in chunks (in pages, if continuous is 'false'),
   * which are created until the total PDF file has been generated. This is a wrapper function
   * for getPDFByElement which helps ensure that a document exists on the page where the
   * function is called.
   *
   * @param id (!required): ID of the HTML element which will be converted into a PDF file.
   *
   * @param options: Set of key-value pairs which hold optional values which differ from the
   *                 default. Any values set in options will override their default counterparts.
   *                 Any values not set in options will be set to the default value. See the
   *                 description for getPDFByElement for a detailed list of possible options.
   *
   * @returns A PDF file which has been generated from the given element into the specified
   *          output type.
   */
  getPDFById(id, options) {
    const element = document.getElementById(id);
    if (element) {
      return this.getPDFByElement(element, options);
    } else {
      return null; // returns null if the given element does not exist on the page
    }
  }

  /**
   * getPDFByElement converts an element and its contents into a pdf by creating a canvas,
   * drawing the content of the element onto the canvas, and then inserting the produced image
   * into a generated pdf file. The function does this in chunks (in pages, if continuous is
   * 'false'), which are created until the total PDF file has been generated. An important
   * consideration when using this function instead of getPDFById is that it CANNOT generate a
   * PDF from an HTML element which is not rendered on the page which the function is called on.
   *
   * @param element (!required): The HTML element which will be converted into a PDF file.
   *
   * @param options: Set of key-value pairs which hold optional values which differ from the
   *                 default. Any values set in options will override their default counterparts.
   *                 Any values not set in options will be set to the default value.
   *                 Possible values are:
   *
   *        @filename: String which specifies the name of the PDF. Defaults to 'output.pdf'.
   *
   *        @outputType: String which determines how the produced PDF will be stored. For
   *        further reference, see the jsPDF documentation for the 'output' function:
   *
   *            http://raw.githack.com/MrRio/jsPDF/master/docs/jsPDF.html#output
   *
   *        @format: String or tuple which describes the dimensions (width and height) of a page
   *        in generated PDF document. Defaults to 'automatic'.
   *
   *        @dpi: Number which specifies the resolution of the produced PDF. Defaults to 100.
   *
   *        @compression: String which dictates the level of compression, options are 'NONE',
   *        'FAST', 'MEDIUM', or 'SLOW'. The faster the compression, the larger the file size
   *        will be. Defaults to 'FAST'.
   *
   *        @continuous: Boolean which dictates whether the rendered chunks of the given element
   *        will be inserted onto the PDF as individual pages or a single, continuous document.
   *        Defaults to 'true'.
   *
   *        @bottomMargin: Number which determines the amount of additional image captured
   *        when rendering chunks. A non-zero value here allows for text in an element to be
   *        rendered at the bottom of one page/chunk and then at the top of the next page/chunk.
   *        Advised to only use when continuous is set to false. Default is 0.
   *
   *        @scrollOffset: Number which determines the scroll value used when generating the
   *        initial canvas that pages/chunks are drawn from. If the element being converted into
   *        a PDF does not have its position set as fixed, it is best to set scrollOffset to be
   *        the scrollY value of the element (Example: { scrollOffset: element.scrollY }). The
   *        default value is 0.
   *
   *        @imageScale: Number which modifies the relative size of drawn content to the overall
   *        size of the page it is rendered onto. Must be greater than 0 and less or equal to 1.
   *        Default value is 0.95.
   *
   *        @imageQuality: Number which determines the overall quality of the lossy compression
   *        used by the images created from the element/canvas before they are placed on the
   *        PDF. Must be between 1 and 0, where higher values yield higher image quality. The
   *        default value is 0.96
   *
   * @returns A PDF file which has been generated from the given element into the specified
   *          output type.
   */
  async getPDFByElement(element, options = undefined) {
    const sizeRestriction = 14400; // pages should not be wider or taller than 14400 px due to PDF restrictions
    const page = this._documentSettings.page;
    const image = this._documentSettings.image;
    const opts = this.options;

    if (options !== undefined && validateOptions(options)) {
      const keys = Object.keys(options);
      keys.forEach((key) => opts[key] = options[key]);
    }

    if (typeof window !== 'undefined') {
      const html2canvas = await import('html2canvas');
      const JsPDF = (await import('jspdf')).default;

      image.source = await html2canvas.default(element, {scale: 1, scrollY: opts.scrollOffset}).then(canvas => canvas);

      // Generate the page dimensions based on the set format and dpi
      const result = resolveFormat(opts.format, element, opts.dpi);
      page.width = ((result[0] * opts.dpi) < sizeRestriction)
        ? (result[0] * opts.dpi)
        : sizeRestriction;
      page.height = ((result[1] * opts.dpi) < sizeRestriction)
        ? (result[1] * opts.dpi)
        : sizeRestriction;
      page.ratio = (page.height / page.width);

      // Generate image settings based on the element
      image.width = element.offsetWidth;
      image.height = image.width * page.ratio;

      // Determine extra page margin depending on whether exact margins have been selected
      image.height = image.height + (image.height * opts.bottomMargin);
      page.height = page.height + (page.height * opts.bottomMargin);

      // Create the base PDF document, depending on whether the document will have pages, or be a single, continuous file
      let pdf = new JsPDF('p', 'pt', [page.width, page.height]);
      if (opts.continuous) {
        pdf = new JsPDF('p', 'pt', [page.width, page.height * (element.clientHeight / image.height)]);
      }

      // Begin generating the PDF
      for (let i = 0; i <= element.clientHeight / image.height; i++) {
        image.yStart = image.height * i; // Shift the starting height, based on the current chunk

        let adjustedImgHeight = image.height;
        let adjustedPageHeight = page.height;
        if (image.yStart + image.height > element.clientHeight) {
          adjustedImgHeight = element.clientHeight - (image.yStart);
          adjustedPageHeight = page.height * (adjustedImgHeight / image.height);
        }

        window.pageCanvas = document.createElement('canvas');
        window.pageCanvas.setAttribute('width', page.width);
        window.pageCanvas.setAttribute('height', page.height);

        const ctx = window.pageCanvas.getContext('2d');
        ctx.drawImage(
          image.source,
          image.xStart,
          image.yStart,
          image.width,
          (adjustedImgHeight),
          image.xOrigin,
          image.yOrigin,
          page.width,
          adjustedPageHeight,
        );
        const canvasDataURL = window.pageCanvas.toDataURL('image/jpeg', opts.imageQuality);

        const margin = (1 - opts.imageScale) / 2;
        if (i > 0 && !opts.continuous) { // Create a new page each chunk, if not a continuous document
          pdf.addPage(page.width, page.height);
          pdf.setPage(i + 1);
          pdf.addImage(
            canvasDataURL,
            'PNG',
            page.width * margin,
            page.height * margin,
            page.width * opts.imageScale,
            page.height * opts.imageScale,
            undefined,
            opts.compression,
          );
        } else { // Do not create new pages if the document is continuous
          pdf.addImage(
            canvasDataURL,
            'PNG',
            page.width * margin,
            (page.height * opts.imageScale * i) - (i * margin),
            page.width * opts.imageScale,
            page.height * opts.imageScale,
            undefined,
            opts.compression,
          );
        }
      }
      return pdf.output(opts.outputType, opts.filename);
    }
    return null;
  }
}
