const mock = jest.fn().mockImplementation(() => {
  return {
    output: jest.fn(() => true),
  };
});

export default mock;