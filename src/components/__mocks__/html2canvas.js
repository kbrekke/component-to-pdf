export default function (element) {
  return new Promise((resolve, reject) => {
    window.canvas = document.createElement('canvas');
    element
      ? resolve(window.canvas)
      : reject('Bad Request');
  });
}