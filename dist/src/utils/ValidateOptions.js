'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.default = validateOptions;

var _DocumentGeneratorErrors = require('../constants/DocumentGeneratorErrors');

function validateOptions(options) {
  if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) !== 'object' && options) {
    throw _DocumentGeneratorErrors.OptionErrors.invalidType(typeof options === 'undefined' ? 'undefined' : _typeof(options));
  } else if (!options) {
    return false;
  }

  var keys = Object.keys(options);

  keys.forEach(function (key) {
    switch (key) {
      case 'filename':
        var filename = options.filename;

        if (typeof filename !== 'string') {
          throw _DocumentGeneratorErrors.OptionErrors.filename.invalidType(typeof filename === 'undefined' ? 'undefined' : _typeof(filename));
        }

        var extension = filename.split('.')[1];

        if (extension !== 'pdf') {
          throw _DocumentGeneratorErrors.OptionErrors.filename.badExtension(filename);
        }
        break;

      case 'outputType':
        var outputType = options[key];

        if (typeof outputType !== 'string') {
          throw _DocumentGeneratorErrors.OptionErrors.outputType.invalidType(typeof outputType === 'undefined' ? 'undefined' : _typeof(outputType));
        }

        var validOutputTypes = ['arraybuffer', 'blob', 'bloburi', 'bloburl', 'datauristring', 'dataurlstring', 'datauri', 'dataurl', 'dataurlnewwindow', 'pdfobjectnewwindow', 'pdfjsnewwindow', 'save'];

        if (!validOutputTypes.includes(outputType)) {
          throw _DocumentGeneratorErrors.OptionErrors.outputType.invalidOutput(outputType);
        }
        break;

      case 'format':
        var format = options[key];

        if (typeof format !== 'string' && !Array.isArray(format)) {
          throw _DocumentGeneratorErrors.OptionErrors.format.invalidType(typeof format === 'undefined' ? 'undefined' : _typeof(format));
        }

        var formatStrings = ['automatic', 'letter', 'a4', 'ledger', 'tabloid', 'a3', 'a2', 'a1', 'a0', 'half-letter', 'legal', 'junior-legal'];

        if (typeof format === 'string' && !formatStrings.includes(format)) {
          throw _DocumentGeneratorErrors.OptionErrors.format.invalidFormatString(format);
        } else if (Array.isArray(format) && format.length !== 2) {
          throw _DocumentGeneratorErrors.OptionErrors.format.invalidFormatArray(format);
        }
        break;

      case 'dpi':
        var dpi = options[key];

        if (typeof dpi !== 'number') {
          throw _DocumentGeneratorErrors.OptionErrors.dpi.invalidType(typeof dpi === 'undefined' ? 'undefined' : _typeof(dpi));
        } else if (!Number.isInteger(dpi)) {
          throw _DocumentGeneratorErrors.OptionErrors.dpi.invalidNumber(dpi);
        } else if (dpi < 1) {
          throw _DocumentGeneratorErrors.OptionErrors.dpi.invalidRange(dpi);
        }
        break;

      case 'compression':
        var compression = options[key];
        var compressionStrings = ['NONE', 'FAST', 'MEDIUM', 'SLOW'];

        if (typeof compression !== 'string') {
          throw _DocumentGeneratorErrors.OptionErrors.compression.invalidType(typeof compression === 'undefined' ? 'undefined' : _typeof(compression));
        } else if (!compressionStrings.includes(compression)) {
          throw _DocumentGeneratorErrors.OptionErrors.compression.invalidCompression(compression);
        }
        break;

      case 'continuous':
        var continuous = options[key];
        if (typeof continuous !== 'boolean') {
          throw _DocumentGeneratorErrors.OptionErrors.continuous(typeof continuous === 'undefined' ? 'undefined' : _typeof(continuous));
        }
        break;

      case 'bottomMargin':
        var bottomMargin = options[key];
        if (typeof bottomMargin !== 'number') {
          throw _DocumentGeneratorErrors.OptionErrors.bottomMargin(typeof bottomMargin === 'undefined' ? 'undefined' : _typeof(bottomMargin));
        }
        break;

      case 'scrollOffset':
        var scrollOffset = options[key];
        if (typeof scrollOffset !== 'number') {
          throw _DocumentGeneratorErrors.OptionErrors.scrollOffset(typeof scrollOffset === 'undefined' ? 'undefined' : _typeof(scrollOffset));
        }
        break;

      case 'imageScale':
        var imageScale = options[key];

        if (typeof imageScale !== 'number') {
          throw _DocumentGeneratorErrors.OptionErrors.imageScale.invalidType(typeof imageScale === 'undefined' ? 'undefined' : _typeof(imageScale));
        } else if (imageScale <= 0 || imageScale > 1) {
          throw _DocumentGeneratorErrors.OptionErrors.imageScale.invalidRange(imageScale);
        }
        break;

      case 'imageQuality':
        var imageQuality = options[key];

        if (typeof imageQuality !== 'number') {
          throw _DocumentGeneratorErrors.OptionErrors.imageQuality.invalidType(typeof imageQuality === 'undefined' ? 'undefined' : _typeof(imageQuality));
        } else if (imageQuality <= 0 || imageQuality > 1) {
          throw _DocumentGeneratorErrors.OptionErrors.imageQuality.invalidRange(imageQuality);
        }
        break;

      default:
        throw _DocumentGeneratorErrors.OptionErrors.badOption(key);
    }
  });
  return true;
}