'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ResolveFormat = require('../utils/ResolveFormat');

var _ResolveFormat2 = _interopRequireDefault(_ResolveFormat);

var _ValidateOptions = require('../utils/ValidateOptions');

var _ValidateOptions2 = _interopRequireDefault(_ValidateOptions);

var _DefaultOptions = require('../constants/DefaultOptions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DocumentGenerator = function () {
  function DocumentGenerator() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

    _classCallCheck(this, DocumentGenerator);

    this._documentSettings = {
      image: {
        source: undefined,
        xStart: 0,
        yStart: 0,
        width: 0,
        height: 0,
        xOrigin: 0,
        yOrigin: 0
      },
      page: {
        height: 0,
        width: 0,
        ratio: 0
      }
    };

    this.options = _DefaultOptions.DefaultOptions;
    if (options) {
      this.setOptions(options);
    }
  }

  _createClass(DocumentGenerator, [{
    key: 'setOptions',
    value: function setOptions(options) {
      var _this = this;

      if ((0, _ValidateOptions2.default)(options)) {
        var keys = Object.keys(options);
        keys.forEach(function (key) {
          return _this.options[key] = options[key];
        });
      }
    }

    /**
     * getPDFById converts an element and its contents into a pdf by creating a canvas, drawing the
     * content of the element onto the canvas, and then inserting the produced image into a
     * generated pdf file. The function does this in chunks (in pages, if continuous is 'false'),
     * which are created until the total PDF file has been generated. This is a wrapper function
     * for getPDFByElement which helps ensure that a document exists on the page where the
     * function is called.
     *
     * @param id (!required): ID of the HTML element which will be converted into a PDF file.
     *
     * @param options: Set of key-value pairs which hold optional values which differ from the
     *                 default. Any values set in options will override their default counterparts.
     *                 Any values not set in options will be set to the default value. See the
     *                 description for getPDFByElement for a detailed list of possible options.
     *
     * @returns A PDF file which has been generated from the given element into the specified
     *          output type.
     */

  }, {
    key: 'getPDFById',
    value: function getPDFById(id, options) {
      var element = document.getElementById(id);
      if (element) {
        return this.getPDFByElement(element, options);
      } else {
        return null; // returns null if the given element does not exist on the page
      }
    }

    /**
     * getPDFByElement converts an element and its contents into a pdf by creating a canvas,
     * drawing the content of the element onto the canvas, and then inserting the produced image
     * into a generated pdf file. The function does this in chunks (in pages, if continuous is
     * 'false'), which are created until the total PDF file has been generated. An important
     * consideration when using this function instead of getPDFById is that it CANNOT generate a
     * PDF from an HTML element which is not rendered on the page which the function is called on.
     *
     * @param element (!required): The HTML element which will be converted into a PDF file.
     *
     * @param options: Set of key-value pairs which hold optional values which differ from the
     *                 default. Any values set in options will override their default counterparts.
     *                 Any values not set in options will be set to the default value.
     *                 Possible values are:
     *
     *        @filename: String which specifies the name of the PDF. Defaults to 'output.pdf'.
     *
     *        @outputType: String which determines how the produced PDF will be stored. For
     *        further reference, see the jsPDF documentation for the 'output' function:
     *
     *            http://raw.githack.com/MrRio/jsPDF/master/docs/jsPDF.html#output
     *
     *        @format: String or tuple which describes the dimensions (width and height) of a page
     *        in generated PDF document. Defaults to 'automatic'.
     *
     *        @dpi: Number which specifies the resolution of the produced PDF. Defaults to 100.
     *
     *        @compression: String which dictates the level of compression, options are 'NONE',
     *        'FAST', 'MEDIUM', or 'SLOW'. The faster the compression, the larger the file size
     *        will be. Defaults to 'FAST'.
     *
     *        @continuous: Boolean which dictates whether the rendered chunks of the given element
     *        will be inserted onto the PDF as individual pages or a single, continuous document.
     *        Defaults to 'true'.
     *
     *        @bottomMargin: Number which determines the amount of additional image captured
     *        when rendering chunks. A non-zero value here allows for text in an element to be
     *        rendered at the bottom of one page/chunk and then at the top of the next page/chunk.
     *        Advised to only use when continuous is set to false. Default is 0.
     *
     *        @scrollOffset: Number which determines the scroll value used when generating the
     *        initial canvas that pages/chunks are drawn from. If the element being converted into
     *        a PDF does not have its position set as fixed, it is best to set scrollOffset to be
     *        the scrollY value of the element (Example: { scrollOffset: element.scrollY }). The
     *        default value is 0.
     *
     *        @imageScale: Number which modifies the relative size of drawn content to the overall
     *        size of the page it is rendered onto. Must be greater than 0 and less or equal to 1.
     *        Default value is 0.95.
     *
     *        @imageQuality: Number which determines the overall quality of the lossy compression
     *        used by the images created from the element/canvas before they are placed on the
     *        PDF. Must be between 1 and 0, where higher values yield higher image quality. The
     *        default value is 0.96
     *
     * @returns A PDF file which has been generated from the given element into the specified
     *          output type.
     */

  }, {
    key: 'getPDFByElement',
    value: function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(element) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
        var sizeRestriction, page, image, opts, keys, html2canvas, JsPDF, result, pdf, i, adjustedImgHeight, adjustedPageHeight, ctx, canvasDataURL, margin;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                sizeRestriction = 14400; // pages should not be wider or taller than 14400 px due to PDF restrictions

                page = this._documentSettings.page;
                image = this._documentSettings.image;
                opts = this.options;


                if (options !== undefined && (0, _ValidateOptions2.default)(options)) {
                  keys = Object.keys(options);

                  keys.forEach(function (key) {
                    return opts[key] = options[key];
                  });
                }

                if (!(typeof window !== 'undefined')) {
                  _context.next = 27;
                  break;
                }

                _context.next = 8;
                return import('html2canvas');

              case 8:
                html2canvas = _context.sent;
                _context.next = 11;
                return import('jspdf');

              case 11:
                JsPDF = _context.sent.default;
                _context.next = 14;
                return html2canvas.default(element, { scale: 1, scrollY: opts.scrollOffset }).then(function (canvas) {
                  return canvas;
                });

              case 14:
                image.source = _context.sent;


                // Generate the page dimensions based on the set format and dpi
                result = (0, _ResolveFormat2.default)(opts.format, element, opts.dpi);

                page.width = result[0] * opts.dpi < sizeRestriction ? result[0] * opts.dpi : sizeRestriction;
                page.height = result[1] * opts.dpi < sizeRestriction ? result[1] * opts.dpi : sizeRestriction;
                page.ratio = page.height / page.width;

                // Generate image settings based on the element
                image.width = element.offsetWidth;
                image.height = image.width * page.ratio;

                // Determine extra page margin depending on whether exact margins have been selected
                image.height = image.height + image.height * opts.bottomMargin;
                page.height = page.height + page.height * opts.bottomMargin;

                // Create the base PDF document, depending on whether the document will have pages, or be a single, continuous file
                pdf = new JsPDF('p', 'pt', [page.width, page.height]);

                if (opts.continuous) {
                  pdf = new JsPDF('p', 'pt', [page.width, page.height * (element.clientHeight / image.height)]);
                }

                // Begin generating the PDF
                for (i = 0; i <= element.clientHeight / image.height; i++) {
                  image.yStart = image.height * i; // Shift the starting height, based on the current chunk

                  adjustedImgHeight = image.height;
                  adjustedPageHeight = page.height;

                  if (image.yStart + image.height > element.clientHeight) {
                    adjustedImgHeight = element.clientHeight - image.yStart;
                    adjustedPageHeight = page.height * (adjustedImgHeight / image.height);
                  }

                  window.pageCanvas = document.createElement('canvas');
                  window.pageCanvas.setAttribute('width', page.width);
                  window.pageCanvas.setAttribute('height', page.height);

                  ctx = window.pageCanvas.getContext('2d');

                  ctx.drawImage(image.source, image.xStart, image.yStart, image.width, adjustedImgHeight, image.xOrigin, image.yOrigin, page.width, adjustedPageHeight);
                  canvasDataURL = window.pageCanvas.toDataURL('image/jpeg', opts.imageQuality);
                  margin = (1 - opts.imageScale) / 2;

                  if (i > 0 && !opts.continuous) {
                    // Create a new page each chunk, if not a continuous document
                    pdf.addPage(page.width, page.height);
                    pdf.setPage(i + 1);
                    pdf.addImage(canvasDataURL, 'PNG', page.width * margin, page.height * margin, page.width * opts.imageScale, page.height * opts.imageScale, undefined, opts.compression);
                  } else {
                    // Do not create new pages if the document is continuous
                    pdf.addImage(canvasDataURL, 'PNG', page.width * margin, page.height * opts.imageScale * i - i * margin, page.width * opts.imageScale, page.height * opts.imageScale, undefined, opts.compression);
                  }
                }
                return _context.abrupt('return', pdf.output(opts.outputType, opts.filename));

              case 27:
                return _context.abrupt('return', null);

              case 28:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getPDFByElement(_x2) {
        return _ref.apply(this, arguments);
      }

      return getPDFByElement;
    }()
  }]);

  return DocumentGenerator;
}();

exports.default = DocumentGenerator;