'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var DefaultOptions = exports.DefaultOptions = {
  filename: 'output.pdf',
  outputType: 'save',
  format: 'automatic',
  dpi: 100,
  compression: 'FAST',
  continuous: true,
  bottomMargin: 0,
  scrollOffset: 0,
  imageScale: 0.95,
  imageQuality: 0.96
};