'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function invalidOptionTypeException(opt, expected, invalid) {
  return new Error('Invalid Type: option \'' + opt + '\' is expected to be of type \'' + expected + '\' but was found to be of type \'' + invalid + '\' instead');
}

var OptionErrors = {
  filename: {
    badExtension: function badExtension(filename) {
      return new Error('Bad Extension: \'' + filename + '\' is not a valid file name, because it must end in \'.pdf\'');
    },
    invalidType: function invalidType(_invalidType) {
      return invalidOptionTypeException('filename', 'string', _invalidType);
    }
  },
  outputType: {
    invalidOutput: function invalidOutput(outputType) {
      return new Error('Invalid Output Type: \'' + outputType + '\' is not a recognized output type');
    },
    invalidType: function invalidType(_invalidType2) {
      return invalidOptionTypeException('outputType', 'string', _invalidType2);
    }
  },
  format: {
    invalidFormatArray: function invalidFormatArray(format) {
      return new Error('Invalid Format Array: \'' + format + '\' is not a valid format, expected to be [\'number\', \'number\']');
    },
    invalidFormatString: function invalidFormatString(format) {
      return new Error('Invalid Format String: \'' + format + '\' is not a recognized format type');
    },
    invalidType: function invalidType(_invalidType3) {
      return new Error('Invalid Type: option \'format\' is expected to be of type \'string\' or of type \'number[]\', but was found to be of type \'' + _invalidType3 + '\' instead');
    }
  },
  dpi: {
    invalidRange: function invalidRange(dpi) {
      return new Error('Invalid Range: ' + dpi + ' is not valid dpi because it must be greater than or equal to 1');
    },
    invalidNumber: function invalidNumber(dpi) {
      return new Error('Invalid Number: ' + dpi + ' is not a valid dpi because it must be an integer');
    },
    invalidType: function invalidType(_invalidType4) {
      return invalidOptionTypeException('dpi', 'number', _invalidType4);
    }
  },
  compression: {
    invalidCompression: function invalidCompression(compression) {
      return new Error('Invalid Compression: \'' + compression + '\' is not a recognized compression type');
    },
    invalidType: function invalidType(_invalidType5) {
      return invalidOptionTypeException('compression', 'string', _invalidType5);
    }
  },
  continuous: function continuous(invalidType) {
    return invalidOptionTypeException('continuous', 'boolean', invalidType);
  },
  bottomMargin: function bottomMargin(invalidType) {
    return invalidOptionTypeException('bottomMargin', 'number', invalidType);
  },
  scrollOffset: function scrollOffset(invalidType) {
    return invalidOptionTypeException('scrollOffset', 'number', invalidType);
  },
  imageScale: {
    invalidRange: function invalidRange(imageScale) {
      return new Error('Invalid Range: ' + imageScale + ' is not a valid imageScale because it must be between 0 and 1');
    },
    invalidType: function invalidType(_invalidType6) {
      return invalidOptionTypeException('imageScale', 'number', _invalidType6);
    }
  },
  imageQuality: {
    invalidRange: function invalidRange(imageQuality) {
      return new Error('Invalid Range: ' + imageQuality + ' is not a valid imageQuality because it must be between 0 and 1');
    },
    invalidType: function invalidType(_invalidType7) {
      return invalidOptionTypeException('imageQuality', 'number', _invalidType7);
    }
  },
  badOption: function badOption(option) {
    return new Error('Bad Option: \'' + option + '\' is not a valid option, but was provided as one');
  },
  invalidType: function invalidType(_invalidType8) {
    return new Error('Invalid Type: generator options must be a set of key value pairs, but were instead found to be of type \'' + _invalidType8 + '\'');
  }
};
exports.OptionErrors = OptionErrors;